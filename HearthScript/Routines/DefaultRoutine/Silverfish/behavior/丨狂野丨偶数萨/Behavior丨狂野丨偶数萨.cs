using System.Collections.Generic;
using System;

namespace HREngine.Bots
{
    public partial class Behavior丨狂野丨偶数萨 : Behavior
    {
        private int bonus_enemy = 4;
        private int bonus_mine = 4;

        public override string BehaviorName() { return "丨狂野丨偶数萨"; }
        PenalityManager penman = PenalityManager.Instance;

        public override int getComboPenality(CardDB.Card card, Minion target, Playfield p, Handmanager.Handcard nowHandcard)
        {
            // 无法选中
            if (target != null && target.untouchable)
            {
                return 100000;
            }
            // 初始惩罚值
            int pen = 0;
            switch (card.nameCN)
            {
				// case CardDB.cardNameCN.锈烂蝰蛇:
				// 	if(p.enemyWeapon.Durability > 0)		//敌方头上有武器
				// 		pen -= (p.enemyWeapon.Angr * bonus_mine) + (p.enemyWeapon.Durability * bonus_mine);   // 惩罚值和攻击力成正比 再加上耐久
				// 	break;
                case CardDB.cardNameCN.乐坛灾星玛加萨:  
                    if (p.owncards.Count >= 5)
                        pen += 6 * bonus_mine;
                    if (p.owncards.Count <= 2)
                        pen -= 2 * bonus_mine;
                    pen += p.owncards.Count;
                    break;
                case CardDB.cardNameCN.腐鳃:  
                    pen -= p.ownMinions.Count * bonus_mine;
                    break;
                case CardDB.cardNameCN.后台保镖:   //2
                    if (target == null || !target.own || target.Angr >= 4 || target.Hp >= 5)
                        return 10000;
                    pen -= 4 * bonus_mine - (target.Angr + target.Hp);    // 属性越小的应该优先使用
                    break;
                case CardDB.cardNameCN.已注能饥饿的愚人:  
                    if (p.owncards.Count <= 6)
                        pen -= 6 * bonus_mine;
                    else
                        pen += 10 * bonus_mine;
                    break;
                case CardDB.cardNameCN.饥饿的愚人:  
                    pen += 10 * bonus_mine;         //没有注能时不推荐使用
                    break;
                case CardDB.cardNameCN.错误产物:
                case CardDB.cardNameCN.鱼群聚集:
                case CardDB.cardNameCN.食人鱼集群:
                    break;
                case CardDB.cardNameCN.已注能图腾物证:
                    if (p.ownMinions.Count <= 4)
                        pen -= 6 * bonus_mine;
                    else
                        pen += 10 * bonus_mine;
                    break;
                case CardDB.cardNameCN.图腾物证:
                    pen += 10 * bonus_mine;         //没有注能时不推荐使用
                    break;
                case CardDB.cardNameCN.巡游向导:
                    if (p.ownMaxMana == 1)          //一费出牌最合适
                        pen -= 2 * bonus_mine;
                    break;
                case CardDB.cardNameCN.嗜血:
                    if (p.ownMinions.Count < 3)
                        pen += 20;
                    pen -= p.ownMinions.Count * bonus_mine;
                    break;
                // =======================================
                case CardDB.cardNameCN.吉恩格雷迈恩:
                    if (p.enemyMinions.Count == 0 && p.ownMaxMana == 6)     //敌方没有随从，正好法力值等于5时，可以出，其余这玩意儿只有在空闲没有其他牌好出的时候出。
                        pen -= 2 * bonus_mine;
                    break;
                case CardDB.cardNameCN.锻石师:
                    pen -= 2000;
                    break;
                case CardDB.cardNameCN.图腾之力:
                case CardDB.cardNameCN.图腾潮涌:
                    foreach (Handmanager.Handcard hhc in p.owncards)
                    {
                        if (hhc.card.nameCN == CardDB.cardNameCN.远古图腾)
                        {
                            pen += 10;
                            break;
                        }
                    }
                    int totemCount = 0;
                    foreach (Minion m in p.ownMinions)
                    {
                        if (m.handcard.card.race == CardDB.Race.TOTEM)
                        {
                            totemCount += 1;
                        }
                    }
                    if (totemCount > 2)
                        pen -= 4 * bonus_mine;
                    else
                        pen += 20;
                    break;
                case CardDB.cardNameCN.风怒:
                    if(target == null || !target.own || target.windfury)
                        return 100000;
                    int maxAttack = 0;
                    foreach (Minion m in p.ownMinions)
                    {
                        if (m.windfury)
                            continue;
                        if(m.Angr > maxAttack)
                            maxAttack = m.Angr;
                    }
                    if(maxAttack > 0 && target.Angr >= maxAttack)
                        pen -= 2 * bonus_mine;
                    else
                        pen += 100000;
                    break;
                case CardDB.cardNameCN.衰变:
                    int allAttack = 0;
                    foreach (Minion m in p.enemyMinions)
                    {
                        allAttack += m.Angr;
                    }
                    if(allAttack >= 10)
                        pen -= 5 * bonus_mine;
                    else
                        pen += 10;
                    break;
                case CardDB.cardNameCN.冰霜撕咬:
                    if (target != null && target.Hp == 3) 
                    {
                        pen -= 4 * bonus_mine;
                        if (p.ownMinions.Count > 3)     // 随从多了尽量 打一个让对面加费用，防aoe一波清了。
                        {
                            pen -= 2 * bonus_mine;
                            if (p.ownMaxMana > 4)       // 4费以上aoe 更多。
                                pen -= 2 * bonus_mine;
                        }
                    }
                    break;
                case CardDB.cardNameCN.连环爆裂:
                    if (target == null || target.own)
                        return 100000;
                    break;
                case CardDB.cardNameCN.即兴演奏:
                    if (target == null || !target.own)
                        return 100000;
                    int kill_num = 0;
                    // if (target != null && !target.own)      // 对敌方随从使用即兴演奏时     也不是不可以，但需要慎重。
                    // {
                    //     pen += 12;
                    //     kill_num--;
                    // }
                    if (p.enemyMinions.Count > 0)
                        foreach (Minion mi in p.enemyMinions)
                        {
                            if (mi.Hp <= 1)                   
                            {
                                kill_num++;
                                pen -= mi.Angr * mi.Angr;
                            }
                        }
                    if(kill_num < 3 && p.enemyHeroName == HeroEnum.thief && p.enemyMaxMana < 3) // 遇到贼开局留了即兴演奏3费之前应该再等他至少铺场2个怪才使用。
                        kill_num -= 3;
                    if (p.ownMinions.Count > 0)
                        foreach (Minion mi in p.ownMinions)
                        {
                            if (mi.Hp <= 1)                    
                            {
                                kill_num--;
                                pen += mi.Angr * mi.Angr; 

                            }
                        }
                    pen -= kill_num * 5;
                    break;
                case CardDB.cardNameCN.驻锚图腾:
                    break;
                case CardDB.cardNameCN.深海融合怪:
                    if(target != null && target.handcard.card.race == CardDB.Race.TOTEM)
                        pen -= 10;
                    break;
                case CardDB.cardNameCN.图腾召唤:
                    foreach (Minion m in p.ownMinions)
                    {
                        if (m.handcard.card.nameCN == CardDB.cardNameCN.驻锚图腾)
                        {
                            pen -= 10;
                            break;
                        }
                    }
                    pen -= 8;
                    break;
                case CardDB.cardNameCN.石雕凿刀:
                    if (p.ownWeapon.Durability > 0)
                        return 100000;
                    if(p.anzUsedOwnHeroPower == 0) 
                        pen += 20;
                    pen -= 100;
                    break;
                case CardDB.cardNameCN.分裂战斧:
                    if (p.ownWeapon.Durability > 0)
                        return 100000;
                    if (p.ownMinions.Count > 2 && p.ownMinions.Count < 5)
                        pen -= 100;
                    else
                        pen += 10;
                    break;
                case CardDB.cardNameCN.深渊魔物:
                case CardDB.cardNameCN.图腾巨像:
                    if (nowHandcard.getManaCost(p) <= 2) pen -= bonus_mine * 3;
                    if (nowHandcard.getManaCost(p) >= 5) pen += bonus_mine * 3;
                    break;
                case CardDB.cardNameCN.远古图腾:
                case CardDB.cardNameCN.火舌图腾:
                case CardDB.cardNameCN.阴燃电鳗:
                case CardDB.cardNameCN.笔记能手:
                case CardDB.cardNameCN.异教低阶牧师:
                    break;
            }
            return pen;
        }

        // 发现卡的价值
        public override int getDiscoverVal(CardDB.Card card, Playfield p)
        {
            if (card.race == CardDB.Race.TOTEM) return 5;
            return 0;
        }

        // 核心，场面值
        public override float getPlayfieldValue(Playfield p)
        {
            if (p.value > -200000) return p.value;
            float retval = 0;
            retval += getGeneralVal(p);
            retval += p.owncarddraw * 5;
            retval -= p.lostWeaponDamage;
            // 危险血线
            int hpboarder = 5;
            // 不考虑法强了
            if (p.enemyHeroName == HeroEnum.mage) retval += 2 * p.enemyspellpower;
            // 抢脸血线
            int aggroboarder = 20;
            retval += getHpValue(p, hpboarder, aggroboarder);
            // 出牌序列数量
            int count = p.playactions.Count;
            int ownActCount = 0;
            // 排序问题！！！！
            for (int i = 0; i < count; i++)
            {
                Action a = p.playactions[i];
                ownActCount++;
                switch (a.actionType)
                {
                    // 英雄攻击
                    case actionEnum.attackWithHero:
                        continue;
                    case actionEnum.useHeroPower:
                    case actionEnum.playcard:
                        break;
                    default:
                        continue;
                }
                switch (a.card.card.nameCN)
                {
                    // 排序优先
                    case CardDB.cardNameCN.幸运币:
                        retval -= 6 * i;
                        break;
                    case CardDB.cardNameCN.锻石师:
                        retval -= 4 * i;
                        break;
                    case CardDB.cardNameCN.驻锚图腾:
                        retval -= 2 * i;
                        break;
                    case CardDB.cardNameCN.火舌图腾:
                        retval -= i;
                        break;
                }
            }
            // 对手基本随从交换模拟
            //retval += enemyTurnPen(p);
            retval -= p.lostDamage;
            retval += getSecretPenality(p); // 奥秘的影响
            retval -= p.enemyWeapon.Angr * 3 + p.enemyWeapon.Durability * 3;

            // 特殊：优势防亵渎
            if (retval > 50 && p.enemyHeroStartClass == TAG_CLASS.WARLOCK && p.enemyMinions.Count == 0 && p.ownMinions.Count > 2)
            {
                bool found = false;
                // 从 2 开始防，术士自带一堆 1
                for (int i = 1; i <= 10; i++)
                {
                    found = false;
                    foreach (Minion m in p.ownMinions)
                    {
                        if (m.Hp == i)
                        {
                            retval -= 2 * (i - 1);
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        if (i == 1 ) retval += 10;
                        if (i == 2 ) retval += 10;
                        break;
                    }
                }
            }
            // 特殊处理一费留硬币等炮
            if (Hrtprozis.Instance.gTurn == 2 )
            {
                int flag = 0;
                foreach (Handmanager.Handcard hc in p.owncards)
                {
                    if (hc.card.nameCN == CardDB.cardNameCN.驻锚图腾) flag |= 1;
                    if (hc.card.nameCN == CardDB.cardNameCN.幸运币) flag |= 2;
                }
                if (flag == 3) retval += 200;
            }
            //p.value = retval;
            return retval;
        }


        // 敌方随从价值 主要等于（HP + Angr） * 4 + 4
        public override int getEnemyMinionValue(Minion m, Playfield p)
        {
            bool dieNextTurn = false;
            foreach (Minion mm in p.enemyMinions)
            {
                if (mm.handcard.card.nameCN == CardDB.cardNameCN.末日预言者)
                {
                    dieNextTurn = true;
                    break;
                }
            }
            if (m.destroyOnEnemyTurnEnd || m.destroyOnEnemyTurnStart || m.destroyOnOwnTurnEnd || m.destroyOnOwnTurnStart) dieNextTurn = true;
            if (dieNextTurn)
            {
                return -1;
            }
            if (m.Hp <= 0) return 0;
            int retval = 4;
            if (m.Angr > 0 || p.enemyHeroStartClass == TAG_CLASS.PALADIN || p.enemyHeroStartClass == TAG_CLASS.PRIEST)
                retval += m.Hp * 4;
            retval += m.spellpower * 2;
            if (!m.frozen && (!m.cantAttack || m.handcard.card.nameCN == CardDB.cardNameCN.邪刃豹))
            {
                retval += m.Angr * 4;
                if (m.Angr > 5) retval += 10;
                if (m.windfury) retval += m.Angr * 2;
            }
            if (m.silenced) return retval;

            if (m.taunt) retval += 2;
            if (m.divineshild) retval += m.Angr * 2;
            if (m.divineshild && m.taunt) retval += 5;
            if (m.stealth) retval += 2;

            // 鱼人
            if (m.handcard.card.race == CardDB.Race.MURLOC) retval += bonus_enemy * 4;

            // 剧毒价值两点属性
            if (m.poisonous)
            {
                retval += 8;
            }
            if (m.lifesteal) retval += m.Angr * bonus_enemy * 4;

            int bonus = 4;
            switch (m.handcard.card.nameCN)
            {
                case CardDB.cardNameCN.无证药剂师:
                    if (Probabilitymaker.Instance.enemyGraveyard.ContainsKey(CardDB.cardIDEnum.SW_091t3))
                    {
                        retval += bonus_enemy * 20;
                    }
                    break;
                case CardDB.cardNameCN.聒噪怪:
                    if (m.Spellburst) retval += bonus_enemy * 20;
                    break;
                case CardDB.cardNameCN.安娜科德拉:
                case CardDB.cardNameCN.巫师学徒:
                case CardDB.cardNameCN.圣殿蜡烛商:
                case CardDB.cardNameCN.奔逃的魔刃豹:
                case CardDB.cardNameCN.鲨鱼之灵:
                case CardDB.cardNameCN.火焰术士弗洛格尔:
                    retval += bonus_enemy * 90;
                    break;
                case CardDB.cardNameCN.对空奥术法师:
                case CardDB.cardNameCN.黑眼:
                    retval += bonus_enemy * 20;
                    break;
                // 解不掉游戏结束
                case CardDB.cardNameCN.加基森拍卖师:
                case CardDB.cardNameCN.虚触侍从:
				case CardDB.cardNameCN.驻锚图腾:
				case CardDB.cardNameCN.安保自动机:
				case CardDB.cardNameCN.奇利亚斯:
				case CardDB.cardNameCN.机械跃迁者:
				case CardDB.cardNameCN.健谈的调酒师:
				case CardDB.cardNameCN.豪宅管家俄里翁:
				case CardDB.cardNameCN.盛装歌手:
				case CardDB.cardNameCN.盗版之王托尼:
                case CardDB.cardNameCN.贪婪的书虫:
                case CardDB.cardNameCN.青蛙之灵:
                case CardDB.cardNameCN.农夫:
                case CardDB.cardNameCN.尼鲁巴蛛网领主:
                case CardDB.cardNameCN.前沿哨所:
                case CardDB.cardNameCN.洛萨:
                case CardDB.cardNameCN.考内留斯罗姆:
                case CardDB.cardNameCN.战场军官:
                case CardDB.cardNameCN.大领主弗塔根:
                case CardDB.cardNameCN.伯尔纳锤喙:
                case CardDB.cardNameCN.魅影歹徒:
                case CardDB.cardNameCN.灵魂窃贼:
                case CardDB.cardNameCN.紫罗兰法师:
                case CardDB.cardNameCN.甜水鱼人斥候:
                case CardDB.cardNameCN.原野联络人:
                case CardDB.cardNameCN.狂欢报幕员:
                case CardDB.cardNameCN.塔姆辛罗姆:
                case CardDB.cardNameCN.导师火心:
                case CardDB.cardNameCN.伊纳拉碎雷:
                case CardDB.cardNameCN.暗影珠宝师汉纳尔:
                case CardDB.cardNameCN.伦萨克大王:
                case CardDB.cardNameCN.洛卡拉:
                case CardDB.cardNameCN.布莱恩铜须:
                case CardDB.cardNameCN.观星者露娜:
                case CardDB.cardNameCN.大法师瓦格斯:
                case CardDB.cardNameCN.火妖:
                case CardDB.cardNameCN.下水道渔人:
                case CardDB.cardNameCN.空中炮艇:
                case CardDB.cardNameCN.船载火炮:
                case CardDB.cardNameCN.团伙核心:
                case CardDB.cardNameCN.巡游领队:
                case CardDB.cardNameCN.科卡尔驯犬者:
                case CardDB.cardNameCN.火舌图腾:
                case CardDB.cardNameCN.治疗图腾:
                    retval += bonus_enemy * 8;
                    break;
                case CardDB.cardNameCN.末日预言者:
                    //foreach(Minion mm in p.ownMinions)
                    //{
                    //    retval += (mm.Hp + mm.Angr) * 5;
                    //}
                    break;
                // 不解巨大劣势
                case CardDB.cardNameCN.拍卖师亚克森:
                case CardDB.cardNameCN.法师猎手:
                case CardDB.cardNameCN.萨特监工:
                case CardDB.cardNameCN.甩笔侏儒:
                case CardDB.cardNameCN.精英牛头人酋长金属之神:
                case CardDB.cardNameCN.莫尔杉哨所:
                case CardDB.cardNameCN.凯瑞尔罗姆:
                case CardDB.cardNameCN.鱼人领军:
                case CardDB.cardNameCN.南海船长:
                case CardDB.cardNameCN.坎雷萨德埃伯洛克:
                case CardDB.cardNameCN.人偶大师多里安:
                case CardDB.cardNameCN.暗鳞先知:
                case CardDB.cardNameCN.灭龙弩炮:
                case CardDB.cardNameCN.神秘女猎手:
                case CardDB.cardNameCN.鲨鳍后援:
                case CardDB.cardNameCN.怪盗图腾:
                case CardDB.cardNameCN.矮人神射手:
                case CardDB.cardNameCN.任务达人:
                case CardDB.cardNameCN.战马训练师:
                case CardDB.cardNameCN.相位追猎者:
                case CardDB.cardNameCN.鱼人宝宝车队:
                case CardDB.cardNameCN.科多兽骑手:
                case CardDB.cardNameCN.奥秘守护者:
                case CardDB.cardNameCN.获救的流民:
                case CardDB.cardNameCN.白银之手新兵:
                case CardDB.cardNameCN.低阶侍从:
                    retval += bonus_enemy * 3;
                    break;
                // 算有点用
                case CardDB.cardNameCN.幽灵狼前锋:
                case CardDB.cardNameCN.战斗邪犬:
                case CardDB.cardNameCN.饥饿的秃鹫:
                case CardDB.cardNameCN.法力浮龙:
                case CardDB.cardNameCN.飞刀杂耍者:
                case CardDB.cardNameCN.锈水海盗:
                case CardDB.cardNameCN.大法师安东尼达斯:
                    retval += bonus_enemy * 2;
                    break;
                case CardDB.cardNameCN.疯狂的科学家:
                    retval -= bonus_enemy * 4;
                    break;
                case CardDB.cardNameCN.艾尔文野猪:
                    retval += bonus_enemy * 2;
                    break;
            }
            return retval;
        }

        // 我方随从价值，大致等于主要等于 （HP + Angr） * 4 + 4
        public override int getMyMinionValue(Minion m, Playfield p)
        {
            bool dieNextTurn = false;
            foreach (Minion mm in p.enemyMinions)
            {
                if (mm.handcard.card.nameCN == CardDB.cardNameCN.末日预言者)
                {
                    dieNextTurn = true;
                    break;
                }
            }
            if (m.destroyOnEnemyTurnEnd || m.destroyOnEnemyTurnStart || m.destroyOnOwnTurnEnd || m.destroyOnOwnTurnStart) dieNextTurn = true;
            if (dieNextTurn)
            {
                return -1;
            }
            if (m.Hp <= 0) return 0;
            int retval = 4;
            if (m.Hp <= 0) return 0;
            retval += m.Hp * 4;
            retval += m.Angr * 4;
            if (m.Hp <= 1) retval -= (m.Angr - 1) * 3;
            if (m.Angr <= 1)
            {
                if (m.Hp > 3)
                {
                    retval -= (m.Hp - 3) * 4;
                }
            }
            // 高攻低血是垃圾
            if (m.Angr > m.Hp + 4) retval -= (m.Angr - m.Hp) * 3;

            // 风怒价值
            if ((!m.playedThisTurn || m.rush == 1 || m.charge == 1) && m.windfury) retval += m.Angr;
            // 圣盾价值
            if (m.divineshild) retval += m.Angr * 3;
            // 潜行价值
            if (m.stealth) retval += m.Angr / 2 + 1;
            // 吸血
            if (m.lifesteal) retval += m.Angr / 2 + 1;
            // 圣盾嘲讽
            if (m.divineshild && m.taunt) retval += 4;

            int bonus = 4;
            switch (m.handcard.card.nameCN)
            {
                case CardDB.cardNameCN.驻锚图腾:
                    retval += bonus_mine * 3;
                    break;
            }
            return retval;
        }

        public override int getSirFinleyPriority(List<Handmanager.Handcard> discoverCards)
        {
            
            return -1; //comment out or remove this to set manual priority
            int sirFinleyChoice = -1;
            int tmp = int.MinValue;
            for (int i = 0; i < discoverCards.Count; i++)
            {
                CardDB.cardNameEN name = discoverCards[i].card.nameEN;
                if (SirFinleyPriorityList.ContainsKey(name) && SirFinleyPriorityList[name] > tmp)
                {
                    tmp = SirFinleyPriorityList[name];
                    sirFinleyChoice = i;
                }
            }
            return sirFinleyChoice;
        }
        public override int getSirFinleyPriority(CardDB.Card card)
        {
            return SirFinleyPriorityList[card.nameEN];
        }

        private Dictionary<CardDB.cardNameEN, int> SirFinleyPriorityList = new Dictionary<CardDB.cardNameEN, int>
        {
            //{HeroPowerName, Priority}, where 0-9 = manual priority
            { CardDB.cardNameEN.lesserheal, 0 }, 
            { CardDB.cardNameEN.shapeshift, 6 },
            { CardDB.cardNameEN.fireblast, 7 },
            { CardDB.cardNameEN.totemiccall, 1 },
            { CardDB.cardNameEN.lifetap, 9 },
            { CardDB.cardNameEN.daggermastery, 5 },
            { CardDB.cardNameEN.reinforce, 4 },
            { CardDB.cardNameEN.armorup, 2 },
            { CardDB.cardNameEN.steadyshot, 8 }
        };


        public override int getHpValue(Playfield p, int hpboarder, int aggroboarder)
        {
            int offset_enemy = 0;
            int offset_mine = p.calEnemyTotalAngr() + Hrtprozis.Instance.enemyDirectDmg;

            int retval = 0;
            // 血线安全
            if (p.ownHero.Hp + p.ownHero.armor - offset_mine > hpboarder)
            {
                retval += (5 + p.ownHero.Hp + p.ownHero.armor - offset_mine - hpboarder) ;
            }
            // 快死了
            else if (p.ownHero.Hp + p.ownHero.armor - offset_mine > 0)
            {
                //if (p.nextTurnWin()) retval -= (hpboarder + 1 - p.ownHero.Hp - p.ownHero.armor);
                retval -= 4 * (hpboarder + 1 - p.ownHero.Hp - p.ownHero.armor + offset_mine) * (hpboarder + 1 - p.ownHero.Hp - p.ownHero.armor + offset_mine);
            }
            else
            {
                retval -= 3 * (hpboarder + 1) * (hpboarder + 1) + 100;
            }
            if (p.ownHero.Hp + p.ownHero.armor - offset_mine < 6 && p.ownHero.Hp + p.ownHero.armor - offset_mine > 0)
            {
                retval -= 80 / (p.ownHero.Hp + p.ownHero.armor - offset_mine);
            }
            // 对手血线安全
            if (p.enemyHero.Hp + p.enemyHero.armor + offset_enemy >= aggroboarder)
            {
                retval += 2 * (aggroboarder - p.enemyHero.Hp - p.enemyHero.armor - offset_enemy);
            }
            // 开始打脸
            else
            {
                retval += 4 * (aggroboarder + 1 - p.enemyHero.Hp - p.enemyHero.armor - offset_enemy);
            }
            // 进入斩杀线
            // if (p.enemyHero.Hp + p.enemyHero.armor + offset_enemy <= 5 && p.enemyHero.Hp + p.enemyHero.armor + offset_enemy > 0)
            // {
            //     retval += 300 / (p.enemyHero.Hp + p.enemyHero.armor - offset_enemy);
            // }
            // 场攻+直伤大于对方生命，预计完成斩杀
            if (p.anzEnemyTaunt == 0 && p.calTotalAngr() + p.calDirectDmg(p.mana, false) >= p.enemyHero.Hp + p.enemyHero.armor)
            {
                retval += 2000;
            }
            // 下回合斩杀本回合打脸
            if (p.calDirectDmg(p.ownMaxMana + 1, false, true) >= p.enemyHero.Hp + p.enemyHero.armor)
            {
                retval += 100;
            }
            return retval;
        }


        /// <summary>
        /// 攻击触发的奥秘惩罚
        /// </summary>
        /// <param name="si"></param>
        /// <param name="attacker"></param>
        /// <param name="defender"></param>
        /// <returns></returns>
        public override int getSecretPen_CharIsAttacked(Playfield p, SecretItem si, Minion attacker, Minion defender)
        {
            if (attacker.isHero) return 0;
            int pen = 0;
            // 攻击的基本惩罚
            if (si.canBe_explosive && !defender.isHero)
            {
                pen -= 20;
                //pen += (attacker.Hp + attacker.Angr);
                foreach (SecretItem sii in p.enemySecretList)
                {
                    sii.canBe_explosive = false;
                }
            }
            return pen;
        }

    }
}