using System;
using System.Collections.Generic;
using System.Text;

namespace HREngine.Bots
{
    class Sim_REV_290 : SimTemplate //* 赎罪教堂 Cathedral of Atonement
                                    //使一个随从获得+2/+1。抽一张牌。
                                    //Give a minion +2/+1 and draw a card.
    {
        // 使用者使用该卡牌时，会触发这个方法
        public override void onCardPlay(Playfield p, bool ownplay, Minion target, int choice)
        {
            // 判断是否为友方使用该卡牌
            if (ownplay)
            {
                // 判断目标随从是否存在，是否是友方随从
                if (target != null && target.own)
                {
                    // 给目标随从增加+2攻击力和+1生命值
                    p.minionGetBuffed(target, 2, 1);

                    // 抽一张牌
                    p.drawACard(CardDB.cardNameEN.unknown, ownplay);
                }
            }
        }
    }
}
